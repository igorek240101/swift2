import Foundation;

// Объявляем массив с количеством дней в месяцах
let countDaysInMonth : [Int] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
// Объявляем массив с названиеяями месецев
let monthNames = [  "Январь", "Февраль", "Март",
                    "Апрель", "Май", "Июнь",
                    "Июль", "Август", "Сентябрь",
                    "Октябрь", "Ноябрь", "Декабрь"];
// Вывод первого массива
for days in countDaysInMonth
{
    print(days);
}
// Вывод двух массивов (работает поттому что они одинакового размера)
for i in 0...monthNames.count-1
{
    print("\(monthNames[i]) - \(countDaysInMonth[i])");
}

print();

// Создадим массив кортежей
let months = [  (name : "Январь", days : 31), (name : "Февраль", days : 28), (name : "Март", days : 31),
                (name : "Апрель", days : 30), (name : "Май", days : 31), (name : "Июнь", days : 30),
                (name : "Июль", days : 31), (name : "Август", days : 31), (name : "Сентябрь", days : 30),
                (name : "Октябрь", days : 31), (name : "Ноябрь", days : 30), (name : "Декабрь", days : 31),];
// Выведем массив кортежей  
for month in months
{
    print("\(month.name) - \(month.days)");
}

print();
// Теперь в обратном порядке
for i in 1...months.count
{
    print("\(months[months.count - i].name) - \(months[months.count - i].days)");
}

print();
print("Введите месяц");
var m : Int = Int(readLine()!)!;
if(m >= 1 && m <= 12) // Проверка корректности месяца
{
    m -= 1; // изменим m чтобы использовать  как индекс массива
    print("Введите день");
    let d : Int = Int(readLine()!)!;
    if(d >= 1 && d <= months[m].days) // Проверка корректности дня
    {
        var count : Int = d - 1; // Количество дней с первого числа месяца до искомого
        if(m > 0) // m может оказаться слишком маленьким
        {
            for i in 0...m - 1
            {
                count += months[i].days
            }
        }
        print("\(months[m].name) \(d) наступит через \(count) день/дней/дня")
        // Реализация аналогичного функционала с помощью Date (он может учитытвать вискосный год)
        let askDate : Date = Calendar.current.date(from : DateComponents(year : 2000, month : m + 1, day : d))!;
        let startDate : Date = Calendar.current.date(from : DateComponents(year : 2000, month : 1, day : 1))!;
        let interval : TimeInterval = askDate.timeIntervalSince(startDate);
        print("бонус, работет для високосного года - \(Int(interval / 60 / 60 / 24))");
    }
}


